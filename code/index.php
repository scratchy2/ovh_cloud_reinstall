<?php
require __DIR__ . '/vendor/autoload.php';
use \Ovh\Api;

function loadConfig()
{
	$content = file_get_contents("config.json");
	return json_decode($content);
}

function reinstallLog($message)
{
    $file = getcwd()."/logs/".$_SERVER['REMOTE_ADDR'].".log";
    $logMsg = $_SERVER['REMOTE_ADDR'] . " [".time()."] " . date("D, d M Y H:i:s") . " -> $message\r\n";

    limitFileSize(1024*1024*1,$file);
    file_put_contents($file,$logMsg,FILE_APPEND);
}

function startReinstallOVHCall($config)
{
	reinstallLog("startBuildingOvhCall");
try
{
	$ovh = new Api( $config->ovhAPPKey,  // Application Key
	                $config->ovhSecretKey,  // Application Secret
	                'ovh-eu',      // Endpoint of API OVH Europe (List of available endpoints)
	                $config->ovhConsumerKey); // Consumer Key

	$url = '/cloud/project/'.$config->projectId. '/instance/'.$config->instanceId.'/reinstall';
	$params = ['imageId' => $config->imageID];

	reinstallLog("startingReinstallOvhCall ($url) params(".json_encode($params));

	$result = $ovh->post($url,$params);

	reinstallLog("OvhReinstallResult:". json_encode($result) . "\r\n");
	print_r( $result );
} catch(\Exception $e)
{
	reinstallLog("OVHReinstallFailed: " . $e->getMessage());
}

}

function limitFileSize($sizeInBytes,$file)
{
	if(filesize($file) > $sizeInBytes)
	{
		copy($file,$file.".bak");
		unlink($file);
	}
}

function serveInstallScript($config)
{
$serverHttp = "http://$config->serverIP";
$str = <<<EOF
#!/bin/sh
(
	curl "$serverHttp/?message=startInstall" && \
	apt-get update && \
	apt-get install -y apache2 curl && \
	curl "$serverHttp/?message=installed" && \
	curl "$serverHttp/?readyForReinstall=true"
) || curl "$serverHttp/?message=failed_during_install_of_packages"

EOF;

echo $str;
reinstallLog("serveInitScript");

}

$config = loadConfig();
if(!$config)
{
        echo "missing config.json" . PHP_EOL;
        exit;
}

if(isset($_GET["message"]))
{
	$message = $_GET["message"];
	reinstallLog($message);
}
else if(isset($_GET["readyForReinstall"]))
{
	startReinstallOVHCall($config);
}
else if(isset($_GET["cloudInit"]))
{
	serveInstallScript($config);
}
else
{

$serverHttp = "http://$config->serverIP";
$str = <<<EOF
<h1>Ready for cloud-init!</h1>
<h2>Heres the script (should be placed on the public_cloud server you want to test, currently it supports apt based distros (debian/ubuntu)):</h2>
<pre>
#!/bin/bash
apt-get update && apt-get install -y curl dos2unix
curl -s "$serverHttp/?cloudInit=true" | dos2unix | bash
</pre>

<h2>logs can be found here (serverIP is the ip of the cloud-client):<br></h2>
<br>
$serverHttp/logs/serverIP.log

EOF;
echo $str;
}
